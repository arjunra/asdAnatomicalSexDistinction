#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 10 12:05:16 2017

@author: Arjun Annapureddy


heavily imported from:
http://scikit-learn.org/stable/modules/manifold.html#locally-linear-embedding
http://scikit-learn.org/stable/auto_examples/manifold/plot_lle_digits.html#sphx-glr-auto-examples-manifold-plot-lle-digits-py

"""
from time import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import (manifold, decomposition, ensemble, preprocessing,
                     discriminant_analysis, random_projection,)


n_neighbors = 50; #just a heuristically chosen number

"""Importing data from xlsx files"""

df = pd.read_excel('../Freesurfer_edited.xlsx')
num_samples, num_features = df.shape
num_features -= 2
labels = df.values[:,0] + df.values[:,1]

features = preprocessing.scale(np.delete(df.values, [0,1], 1))


#------------
#Visualize the embedding

def plot_embedding(feature_data, title =None):
    x_min, x_max = np.min(feature_data, 0), np.max(feature_data, 0)
    feature_data = (feature_data - x_min) / (x_max - x_min)
    
    
    plt.figure()
    for i in range(num_samples):
        txt = labels[i][0] + labels[i][-1]
        if txt == 'FD':
            text = 'FA'
            pcolor = 'red'
        elif txt == 'MD':
            text = 'MA'
            pcolor = 'blue'
        elif txt == 'Fl':
            text = 'FT'
            pcolor = 'violet'
        else:
            text = 'MT'
            pcolor = 'lime'
        plt.text(feature_data[i, 0], feature_data[i, 1], text,
                 color=pcolor,
                 fontdict={'weight': 'bold', 'size': 9})

    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)

    
#----------------------------------------------------------------------
# Random 2D projection using a random unitary matrix
print("Computing random projection")
rp = random_projection.GaussianRandomProjection(n_components=2, random_state=42)
projected = rp.fit_transform(features)
plot_embedding(projected, "Random Projection")

#----------------------------------------------------------------------
# Projection on to the first 2 principal components

print("Computing PCA projection")
t0 = time()
pca = decomposition.PCA(n_components=2).fit_transform(features)
plot_embedding(pca,
               "Principal Components projection(time %.2fs)" %
               (time() - t0))

#----------------------------------------------------------------------
# Projection on to the first 2 linear discriminant components

print("Computing Linear Discriminant Analysis projection")
X2 = features.copy()
X2.flat[::features.shape[1] + 1] += 0.01  # Make X invertible
t0 = time()
lda = discriminant_analysis.LinearDiscriminantAnalysis(n_components=2).fit_transform(X2, labels)
plot_embedding(lda,
               "Linear Discriminant projection(time %.2fs)" %
               (time() - t0))


#----------------------------------------------------------------------
# Isomap projection
print("Computing Isomap embedding")
t0 = time()
iso = manifold.Isomap(n_neighbors, n_components=2).fit_transform(features)
print("Done.")
plot_embedding(iso,
               "Isomap projection(time %.2fs)" %
               (time() - t0))


#----------------------------------------------------------------------
# Locally linear embedding
print("Computing LLE embedding")
clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2, method = 'standard')
t0 = time()
lle = clf.fit_transform(features)
print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
plot_embedding(lle,
               "Locally Linear Embedding(time %.2fs)" %
               (time() - t0))


#----------------------------------------------------------------------
# Modified Locally linear embedding 
print("Computing modified LLE embedding")
clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2,
                                      method='modified')
t0 = time()
mlle = clf.fit_transform(features)
print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
plot_embedding(mlle,
               "Modified Locally Linear Embedding(time %.2fs)" %
               (time() - t0))

'''
#----------------------------------------------------------------------
# HLLE embedding
print("Computing Hessian LLE embedding")
clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2,
                                      method='hessian')
t0 = time()
hlle = clf.fit_transform(features)
print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
plot_embedding(hlle,
               "Hessian Locally Linear Embedding(time %.2fs)" %
               (time() - t0))


#----------------------------------------------------------------------
# LTSA embedding
print("Computing LTSA embedding")
clf = manifold.LocallyLinearEmbedding(n_neighbors, n_components=2,
                                      method='ltsa')
t0 = time()
ltsa = clf.fit_transform(features)
print("Done. Reconstruction error: %g" % clf.reconstruction_error_)
plot_embedding(ltsa,
               "Local Tangent Space Alignment(time %.2fs)" %
               (time() - t0))

#----------------------------------------------------------------------'''
# MDS  embedding of the digits dataset
print("Computing MDS embedding")
clf = manifold.MDS(n_components=2, n_init=1, max_iter=100)
t0 = time()
mds = clf.fit_transform(features)
print("Done. Stress: %f" % clf.stress_)
plot_embedding(mds,
               "MDS embedding(time %.2fs)" %
               (time() - t0))

#----------------------------------------------------------------------
# Random Trees embedding
print("Computing Totally Random Trees embedding")
hasher = ensemble.RandomTreesEmbedding(n_estimators=200, random_state=0,
                                       max_depth=5)
t0 = time()
X_transformed = hasher.fit_transform(features)
pca = decomposition.TruncatedSVD(n_components=2)
reduced = pca.fit_transform(X_transformed)

plot_embedding(reduced,
               "Random forest embedding (time %.2fs)" %
               (time() - t0))

#----------------------------------------------------------------------
# Spectral embedding of the digits dataset
print("Computing Spectral embedding")
embedder = manifold.SpectralEmbedding(n_components=2, random_state=0,)
t0 = time()
X_se = embedder.fit_transform(features)

plot_embedding(X_se,
               "Spectral embedding(time %.2fs)" %
               (time() - t0))

#----------------------------------------------------------------------
# t-SNE embedding
print("Computing t-SNE embedding")
tsne = manifold.TSNE(n_components=2, init='pca', random_state=0)
t0 = time()
X_tsne = tsne.fit_transform(features)

plot_embedding(X_tsne,
               "t-SNE embedding(time %.2fs)" %
               (time() - t0))

plt.show()









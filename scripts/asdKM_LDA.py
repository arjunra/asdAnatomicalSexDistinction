#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 13:29:41 2017

@author: Arjun Annapureddy
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import discriminant_analysis
from sklearn.cluster import KMeans
from sklearn.preprocessing import scale
from sklearn import metrics



"""Importing data from xlsx files"""

df = pd.read_excel('../Freesurfer_edited.xlsx')
num_samples, num_features = df.shape
num_features -= 2
labels = df.values[:,0] + df.values[:,1]

#features = np.delete(df.values, [0,1], 1)
features = scale(np.delete(df.values, [0,1], 1))


#------------
#Visualize the embedding

def plot_embedding(feature_data, km_model, title =None):
    x_min, x_max = np.min(feature_data, 0), np.max(feature_data, 0)
    feature_data = (feature_data - x_min) / (x_max - x_min)
    
    
    plt.figure()
    for i in range(num_samples):
        txt = labels[i][0] + labels[i][-1]
        if txt == 'FD':
            pcolor = 'red'
        elif txt == 'MD':
            pcolor = 'blue'
        elif txt == 'Fl':
            pcolor = 'violet'
        else:
            pcolor = 'lime'
        plt.text(feature_data[i, 0], feature_data[i, 1], str(txt),
                 color=pcolor,
                 fontdict={'weight': 'bold', 'size': 9})
    centroids = (km_model.cluster_centers_  - x_min)/(x_max - x_min)  
    plt.scatter(centroids[:, 0], centroids[:, 1],
            marker='x', s=169, linewidths=3,
            color='black', zorder=10)

    #plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)
    plt.show()
#-------------------
#For bar plots

def autolabel(rects):
    """
    Attach a text label above each bar displaying its height
    """
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')    

#----------------------------------------------------------------------  
    
     # Projection on to the first 2 linear discriminant components
print("Computing Linear Discriminant Analysis projection")
X2 = features.copy()
X2.flat[::features.shape[1] + 1] += 0.01  # Make X invertible

lda = discriminant_analysis.LinearDiscriminantAnalysis(n_components=2).fit(X2, labels)

#-------------------------------------------------------------------
# K-Means clustering within projection space

print("Computing K-Means Clustering")

reduced_data = lda.transform(X2)
kmeans = KMeans(init='k-means++', n_clusters=4, n_init=10)
kmeans.fit(reduced_data,labels)

# Step size of the mesh. Decrease to increase the quality of the VQ.
h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

# Plot the decision boundary. For that, we will assign a color to each
x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

# Obtain labels for each point in mesh. Use last trained model.
Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure(1)
plt.clf()
plt.imshow(Z, interpolation='nearest',
           extent=(xx.min(), xx.max(), yy.min(), yy.max()),
           cmap=plt.cm.Paired,
           aspect='auto', origin='lower')

plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
# Plot the centroids as a white X
centroids = kmeans.cluster_centers_
plt.scatter(centroids[:, 0], centroids[:, 1],
            marker='x', s=169, linewidths=3,
            color='w', zorder=10)
plt.title('K-means clustering on the LDA projected data\n'
          'Centroids are marked with white cross')
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
plt.show()


mapping = dic = {'MaleTypical' : 0, 'FemaleASD' : 1, 'FemaleTypical' : 2, 'MaleASD' : 3}
num_labels = [mapping[s] for s in labels]

print('Homgeneity:' + str(metrics.homogeneity_score(num_labels, kmeans.labels_)))
print('Completeness:' + str(metrics.completeness_score(num_labels, kmeans.labels_)))
print('v-meas:' + str(metrics.v_measure_score(num_labels, kmeans.labels_)))
print('a-rand-score:' + str(metrics.adjusted_rand_score(num_labels, kmeans.labels_)))
print('a-mutual-info:' + str(metrics.adjusted_mutual_info_score(num_labels,  kmeans.labels_)))

#-----------
# Interpreting High Impact Features

a0 = lda.scalings_[:,0]
a1 = lda.scalings_[:,1]

top_a0 = sorted(range(len(a0)), reverse = False,key=lambda i: a0[i])[-10:]
top_a1 = sorted(range(len(a1)), reverse = False,key=lambda i: a1[i])[-10:]
bottom_a0 = sorted(range(len(a0)), reverse = True,key=lambda i: a0[i])[-10:]
bottom_a1 = sorted(range(len(a1)), reverse = True,key=lambda i: a1[i])[-10:]


a0_labels = np.append(df.columns[bottom_a0],df.columns[top_a0])
a1_labels = np.append(df.columns[bottom_a1],df.columns[top_a1])
y_pos = np.arange(len(a0_labels))

a0_plt = np.append(a0[bottom_a0],a0[top_a0])
a1_plt = np.append(a1[bottom_a1],a1[top_a1])


plt.figure()


plt.barh(y_pos, a0_plt, align='center', alpha=0.5)
plt.yticks(y_pos, a0_labels)
plt.yticks(rotation=0)
plt.ylabel('Eigen-component')
plt.title('Impact of selected regions to discriminating differences in 1st dimension')

plt.show()


plt.figure()


plt.barh(y_pos, a1_plt, align='center', alpha=0.5)
plt.yticks(y_pos, a1_labels)
plt.yticks(rotation=0)
plt.ylabel('Eigen-component')
plt.title('Impact of selected regions to discriminating differences in 2nd dimension')

plt.show()

#------------
#Interpreting group differences

s_ticks = ['Male', 'Female']
d_ticks = ['ASD', 'Typical']

men_means = []
men_std = []
women_means = []
women_std = []

for i in range(0,3):
    male_group = df[df['subject_gender'] == 'Male']
    
    male_group_b = male_group[a0_labels[i]]
    men_means = np.append(men_means, male_group_b.mean(0))
    men_std = np.append(men_std, male_group_b.std(0))
    
    female_group = df[df['subject_gender'] == 'Female']
    
    female_group_b = female_group[a0_labels[i]]
    women_means = np.append(women_means, female_group_b.mean(0))
    women_std = np.append(women_std, female_group_b.std(0))
    
for i in range(0,3):
    male_group = df[df['subject_gender'] == 'Male']
    
    male_group_b = male_group[a0_labels[-1-i]]
    men_means = np.append(men_means, male_group_b.mean(0))
    men_std = np.append(men_std, male_group_b.std(0))
    
    female_group = df[df['subject_gender'] == 'Female']
    
    female_group_b = female_group[a0_labels[-1-i]]
    women_means = np.append(women_means, female_group_b.mean(0))
    women_std = np.append(women_std, female_group_b.std(0))    
    
   
    
N = 6


ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, men_means, width, color='r', yerr=men_std)

rects2 = ax.bar(ind + width, women_means, width, color='y', yerr=women_std)

# add some text for labels, title and axes ticks
ax.set_ylabel('values')
ax.set_title('values by region and diagnosis')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels((str(a0_labels[0]), str(a0_labels[1]), 
                    str(a0_labels[2]), str(a0_labels[-1]), str(a0_labels[-2]),
                    str(a0_labels[-3])))


ax.legend((rects1[0], rects2[0]), ('Male', 'Female'))




autolabel(rects1)
autolabel(rects2)

plt.show()
    


##-----------TO DO: rename variables to better

s_ticks = ['Male', 'Female']
d_ticks = ['ASD', 'Typical']

men_means = []
men_std = []
women_means = []
women_std = []

for i in range(0,3):
    male_group = df[df['app_diag'] == 'ASD']
    
    male_group_b = male_group[a1_labels[i]]
    men_means = np.append(men_means, male_group_b.mean(0))
    men_std = np.append(men_std, male_group_b.std(0))
    
    female_group = df[df['app_diag'] == 'Typical']
    
    female_group_b = female_group[a1_labels[i]]
    women_means = np.append(women_means, female_group_b.mean(0))
    women_std = np.append(women_std, female_group_b.std(0))
    
for i in range(0,3):
    male_group = df[df['app_diag'] == 'ASD']
    
    male_group_b = male_group[a1_labels[-1-i]]
    men_means = np.append(men_means, male_group_b.mean(0))
    men_std = np.append(men_std, male_group_b.std(0))
    
    female_group = df[df['app_diag'] == 'Typical']
    
    female_group_b = female_group[a1_labels[-1-i]]
    women_means = np.append(women_means, female_group_b.mean(0))
    women_std = np.append(women_std, female_group_b.std(0))    
    
   
    
N = 6


ind = np.arange(N)  # the x locations for the groups
width = 0.35       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, men_means, width, color='r', yerr=men_std)

rects2 = ax.bar(ind + width, women_means, width, color='y', yerr=women_std)

# add some text for labels, title and axes ticks
ax.set_ylabel('values')
ax.set_title('values by region and diagnosis')
ax.set_xticks(ind + width / 2)
ax.set_xticklabels((str(a1_labels[0]), str(a1_labels[1]), 
                    str(a1_labels[2]), str(a1_labels[-1]), str(a1_labels[-2]),
                    str(a1_labels[-3])))


ax.legend((rects1[0], rects2[0]), ('ASD', 'Typical'))



autolabel(rects1)
autolabel(rects2)

plt.show()





